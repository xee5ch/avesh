(in-package #:avesh)

(defcommandhook exec () (command arguments next)
  (when (= (length arguments) 0)
    (error "Not enough arguments to exec."))
  (run-exec (format nil "~{~A~^ ~}" arguments)))

(defcommandhook exit () (command arguments next)
  (when (> (length arguments) 1)
    (error "Too many arguments to exit."))
  (uiop:quit
   (if (first arguments)
       (parse-integer (first arguments))
       0)))

(defvar *children* nil)

(defhook command (:weight 1000) (command arguments next)
  (when (> (length command) 0)
    (let ((pid (sb-posix:fork)))
      (cond
        ((<= pid -1) (error (format nil "Failed to start ~A" command)))
        ((= pid 0) (run-exec (append (list command) arguments)))
        (t (multiple-value-bind (pid status)
               (sb-posix:waitpid pid 0)
             (push pid *children*)
             status)))))
  (funcall next command arguments))

(cffi:defcvar *errno* :int)

(defun empty-string-p (string)
  (string= string ""))

(export 'run-exec)
(defun run-exec (command)
  (cffi:with-foreign-objects ((argv :string (1+ (length command))))
    (dotimes (i (length command))
      ;; Some elements can be symbols or numbers
      (setf (cffi:mem-aref argv :string i) (format nil "~A" (nth i command))))
    ;; Last element must be NULL pointer
    (setf (cffi:mem-aref argv :string (length command)) (cffi:null-pointer))
    (cffi:with-foreign-string (cmd (format nil "~A" (first command)))
      (when (< (cffi:foreign-funcall "execvp"
                                     :pointer cmd
                                     :pointer argv
                                     :int)
               0)
        (error (cffi:foreign-funcall "strerror" :int *errno* :string))))))
