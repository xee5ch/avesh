(defpackage #:avesh.expansion
  (:use #:cl #:avesh))

(in-package #:avesh.expansion)

(asdf:defsystem #:avesh.expansion
  :description "Expansion plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh :cl-ppcre)
  :components ((:file "expansion")))
