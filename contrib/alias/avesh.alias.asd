(defpackage #:avesh.alias
  (:use #:cl #:avesh))

(in-package #:avesh.alias)

(asdf:defsystem #:avesh.alias
  :description "Alias plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh)
  :components ((:file "alias")))
