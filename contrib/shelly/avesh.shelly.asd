(defpackage #:avesh.shelly
  (:use #:cl #:avesh))

(in-package #:avesh.shelly)

(asdf:defsystem #:avesh.shelly
  :description "Shelly plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh :shelly)
  :components ((:file "shelly")))
